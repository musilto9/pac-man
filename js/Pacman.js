// directions: 0 = up, 1 = right, 2 = down, 3 = left

class Pacman {

    constructor() {
        this.game = null;
        this.x = 13;
        this.y = 24;
        this.direction = 3;
        this.offsetX = CELL_SIZE / 2;
        this.offsetY = 0;
        this.killCounter = 0;
        this.dieI = 0;
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.strokeStyle = "transparent";
        ctx.fillStyle = "yellow";
        let x = (this.x + 0.5) * CELL_SIZE + this.offsetX;
        let y = (this.y + 0.5) * CELL_SIZE + this.offsetY;
        let r = 0.7 * CELL_SIZE;
        switch (this.direction) {
            case 0:
                ctx.arc(x, y, r, - 2 * Math.PI / 6 + this.getMouthRadius(0, -1), 8 * Math.PI / 6 + this.getMouthRadius(0, 1));
                break;
            case 1:
                ctx.arc(x, y, r, Math.PI / 6 + this.getMouthRadius(1, -1), -Math.PI / 6 + this.getMouthRadius(1, 1));
                break;
            case 2:
                ctx.arc(x, y, r, - 8 * Math.PI / 6 + this.getMouthRadius(0, -1), Math.PI / 3 + this.getMouthRadius(0, 1));
                break;
            case 3:
                ctx.arc(x, y, r, - 5 * Math.PI / 6 + this.getMouthRadius(1, -1), 5 * Math.PI / 6 + this.getMouthRadius(1, 1));
                break;
        }
        ctx.lineTo((this.x + 0.5)  * CELL_SIZE + this.offsetX,(this.y + 0.5) * CELL_SIZE + this.offsetY);
        ctx.fill();
    }

    move() {
        if (this.isNextCellWall() && this.offsetX === 0 && this.offsetY === 0) return;
        switch(this.direction) {
            case 0:
                this.offsetY--;
                if (this.offsetY <= -CELL_SIZE / 2) {
                    this.y--;
                    this.offsetY = CELL_SIZE / 2;
                }
                break;
            case 1:
                this.offsetX++;
                if (this.offsetX >= CELL_SIZE / 2) {
                    this.x++;
                    this.offsetX = -CELL_SIZE / 2;
                }
                break;
            case 2:
                this.offsetY++;
                if (this.offsetY >= CELL_SIZE / 2) {
                    this.y++;
                    this.offsetY = -CELL_SIZE / 2;
                }
                break;
            case 3:
                this.offsetX--;
                if (this.offsetX <= -CELL_SIZE / 2) {
                    this.x--;
                    this.offsetX = CELL_SIZE / 2;
                }
                break;
        }

        // check if pacman is outside of game field
        if (this.x < 0) this.x = this.game.board.cells[0].length - 1;
        if (this.x >= this.game.board.cells[0].length) this.x = 0;

        // check collisions
        if (this.game.board.isSmallDot(this.x, this.y)) this.eatSmallDot();
        if (this.game.board.isBigDot(this.x, this.y)) this.eatBigDot();
        if (this.game.fruit && this.x === this.game.fruit.x && this.y === this.game.fruit.y) this.eatFruit();

    }

    setDirection(arrow) {
        // checks for walls and then sets direction based on user input
        switch (arrow) {
            case "ArrowUp":
                if (!this.game.board.isWall(this.x, this.y-1) &&
                    (this.direction === 2 || Math.abs(this.offsetX) < CELL_SIZE / 2)) {
                    this.direction = 0;
                    this.offsetX = 0;
                }
                break;
            case "ArrowRight":
                if (!this.game.board.isWall(this.x+1, this.y) &&
                    (this.direction === 3 || Math.abs(this.offsetY) < CELL_SIZE / 2)){
                    this.direction = 1;
                    this.offsetY = 0;
                }
                break;
            case "ArrowDown":
                if (!this.game.board.isWall(this.x, this.y+1) &&
                    (this.direction === 0 || Math.abs(this.offsetX) < CELL_SIZE / 2)) {
                    this.direction = 2;
                    this.offsetX = 0;
                }
                break;
            case "ArrowLeft":
                if (!this.game.board.isWall(this.x-1, this.y) &&
                    (this.direction === 1 || Math.abs(this.offsetY) < CELL_SIZE / 2)) {
                    this.direction = 3;
                    this.offsetY = 0;
                }
                break;
        }
    }

    isNextCellWall() {
        switch(this.direction) {
            case 0:
                return this.game.board.isWall(this.x, this.y-1);
            case 1:
                 return this.game.board.isWall(this.x+1, this.y);
            case 2:
                return this.game.board.isWall(this.x, this.y+1);
            case 3:
                return this.game.board.isWall(this.x-1, this.y);
        }
    }

    // small dot collision sequence
    eatSmallDot() {
        if (playNonLoopedSounds) MUNCH1_AUDIO.play();
        score += 10;
        this.game.board.setCell(this.x, this.y, 1);
        this.game.updateScore();
        this.game.updateDotCounter();
    }

    // big dot (power pellet) collision sequence
    eatBigDot() {
        this.killCounter = 0;
        score += 50;
        this.game.board.setCell(this.x, this.y, 1);
        this.game.updateScore();
        this.game.ghosts.forEach(function (item) {
            item.setFrightened();
        })
    }

    // fruit collision sequence
    eatFruit() {
        if (playNonLoopedSounds) EAT_FRUIT_AUDIO.play();
        score += this.game.fruit.points;
        this.game.updateScore();
        this.game.fruit = null;
    }

    // sets mouth radius based on offset
    getMouthRadius(direction, coef) {
        let num;
        if (direction === 0) num = CELL_SIZE/2 - Math.abs(this.offsetY);
        else num = CELL_SIZE/2 - Math.abs(this.offsetX)
        if (num === 0 || num === 1) num = 1.001;
        return coef * Math.PI / 6 / num;
    }

    // death sequence step
    step() {
        game.drawBackground();
        game.board.draw(game.ctx);
        game.ctx.fillStyle = "yellow";
        game.ctx.strokeStyle = "transparent";
        game.ctx.beginPath();
        game.ctx.arc((game.pacman.x + 0.5) * CELL_SIZE + game.pacman.offsetX,
            (game.pacman.y + 0.5) * CELL_SIZE + game.pacman.offsetY,
            0.7 * CELL_SIZE,
            -Math.PI / 2 + game.pacman.dieI,
            -Math.PI / 2 - game.pacman.dieI)
        game.ctx.lineTo((game.pacman.x + 0.5)  * CELL_SIZE + game.pacman.offsetX,
            (game.pacman.y + 0.5) * CELL_SIZE + game.pacman.offsetY);
        game.ctx.fill();
        game.pacman.dieI += Math.PI / 120;
        if (game.pacman.dieI < Math.PI) window.requestAnimationFrame(game.pacman.step);
        else {
            if (lives === 0) game.loose();
            else {
                lives--;
                game.updateLives();
                game.reset();
            }
        }
    }

    // initiates death sequence
    die() {
        this.dieI = 0;
        stopAllLoopingSounds();
        this.game.timerRunning = false;
        this.game.isPaused = true;
        hideMenu();
        if (playNonLoopedSounds) DEATH_AUDIO.play();
        window.requestAnimationFrame(game.pacman.step);
    }

    // kill ghost sequence
    kill() {
        if (playNonLoopedSounds) EAT_GHOST_AUDIO.play();
        this.killCounter++;
        score += (Math.pow(2, this.killCounter) * 100);
        this.game.updateScore();
    }
}