const CELL_SIZE = 20;
const FRAME_DELAY = 17;

let score = 0;
let lives = 2;
let fruits;

let showDest = false;
let playLoopedSounds = true;
let playNonLoopedSounds = true;

class Game {

    constructor() {
        this.timerRunning = false;
        this.isPaused = false;
        this.board = new Board();
        this.pacman = new Pacman();
        this.ghosts = [new Blinky(), new Pinky(), new Inky(), new Clyde()];
        this.fruit = null;
        this.dotCounter = 0;

        this.canvas = $("#game-canvas");
        this.canvas.attr("height", this.board.cells.length * CELL_SIZE + "px");
        this.canvas.attr("width", this.board.cells[0].length * CELL_SIZE + "px");
        this.ctx = this.canvas[0].getContext("2d");
        this.draw();

        this.updateScore();
        this.updateLives();
    }

    // prepare for new game
    ready() {
        stopAllLoopingSounds();
        if (playNonLoopedSounds) {
            GAME_START_AUDIO.play();
            GAME_START_AUDIO.onended = function () {
                this.play();
            }.bind(this);
        }
        else setTimeout(() => {
            this.play();
        }, 1000);
    }

    // start game
    play() {
        $(window).on("keydown.game", this.handleKeyPress);

        this.updateDotCounter();
        this.pacman.game = this;
        this.ghosts.forEach((item) => {
            item.game = this;
            item.setScatterCoords();
        });

        this.continue();

        // this timer is used for switching between ghost states
        this.timerInterval = setInterval(() => {
            if (this.timerRunning) {
                this.ghosts.forEach((item) => {
                    item.setStateByTimer();
                    item.timer++;
                });
            }
        }, 1000);
    }

    handleKeyPress(e) {
        if (e.key === "Escape") {
            if (game.isPaused) game.continue();
            else game.pause();
        }
        if (!this.isPaused) game.pacman.setDirection(e.key);
    }

    // one step that is executed ca 60 times per second
    step() {
        game.pacman.move();
        game.ghosts.forEach((item) => {
            item.move();
        });
        game.draw();
        playStateSounds();
        if (!game.isPaused) window.requestAnimationFrame(game.step);
    }

    // continue paused game
    continue() {
        // add state to historyAPI
        window.history.pushState({}, '');
        this.timerRunning = true;
        this.isPaused = false;
        window.requestAnimationFrame(game.step);
        hideMenu();
    }

    // pause game
    pause() {
        stopAllLoopingSounds();
        this.timerRunning = false;
        this.isPaused = true;
        showMenu();
    }


    // redraw canvas and all elements
    draw() {
        this.drawBackground();
        this.board.draw(this.ctx);
        if (this.fruit) this.fruit.draw(this.ctx);
        this.pacman.draw(this.ctx);
        this.ghosts.forEach((item) => {
            item.draw(this.ctx);
        });
    }

    drawBackground() {
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(0, 0, this.canvas[0].clientWidth, this.canvas[0].clientHeight);
    }

    updateScore() {
        document.querySelector("#score").innerHTML = score;
        for (let i = 0; i < this.board.cells.length; i++) {
            for (let j = 0; j < this.board.cells[0].length; j++) {
                if (this.board.cells[i][j] === 2) return;
            }
        }
        this.win();
    }

    updateLives() {
        document.querySelector("#lives").innerHTML = lives;
    }

    // dot counter is used to determine when a ghost should leave it's house and when to generate a fruit
    async updateDotCounter() {
        this.dotCounter++;
        if ([70, 170].includes(this.dotCounter)) this.fruit = await this.getRandomFruit();
        this.ghosts.forEach((item) => {
            if (item.dotCounter >= item.dotLimit && item.state === 0 && !item.isLeaving) item.leaveHouse();
        })
        if (this.ghosts[1].state === 0) this.ghosts[1].dotCounter++;
        else if (this.ghosts[2].state === 0) this.ghosts[2].dotCounter++;
        else if (this.ghosts[3].state === 0) this.ghosts[3].dotCounter++;
    }

    // returns random fruit based on probabilities set in fruit-config.json
    async getRandomFruit() {
        if (!fruits) {
            fruits = await loadFile("resources/fruit-config.json").then(res => res["fruits"]);
        }
        let i;
        let probabilities = [];
        for (i = 0; i < fruits.length; i++)
            probabilities[i] = fruits[i]["probability"] + (probabilities[i - 1] || 0);
        let random = Math.random() * probabilities[probabilities.length - 1];
        for (i = 0; i < probabilities.length; i++)
            if (probabilities[i] > random)
                break;
        return new Fruit(fruits[i]["id"]);
    }

    // end game
    end() {
        stopAllLoopingSounds();
        this.timerRunning = false;
        this.isPaused = true;
        clearInterval(this.timerInterval);
    }

    // winning sequence
    win() {
        this.end();
        resetGame(false);
        showMenu();
    }

    // loosing sequence
    loose() {
        this.end();
        resetGame(true);
        showMenu();
    }

    // resets whole game field with all elements and starts game again
    reset() {
        this.pacman.x = 13;
        this.pacman.y = 24;
        this.pacman.direction = 3;
        this.pacman.offsetX = CELL_SIZE / 2;
        this.pacman.offsetY = 0;

        this.ghosts.forEach((item)=> {
            item.offsetX = CELL_SIZE / 2;
            item.offsetY = 0;
            item.state = 0;
            item.speed = 0;
            item.timer = 0;
        })

        this.ghosts[0].x = 13;
        this.ghosts[0].y = 12;
        this.ghosts[0].direction = 3;
        this.ghosts[0].state = 1;
        this.ghosts[0].speed = 1;

        this.ghosts[1].x = 13;
        this.ghosts[1].y = 15;
        this.ghosts[1].direction = 0;

        this.ghosts[2].x = 11;
        this.ghosts[2].y = 15;
        this.ghosts[2].direction = 1;

        this.ghosts[3].x = 15;
        this.ghosts[3].y = 15;
        this.ghosts[3].direction = 3;

        this.draw();

        setTimeout(() => {
            this.continue()
            this.updateDotCounter();
        }, 2000)
    }
}

