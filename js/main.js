let game;
let menu;
let main;
let name;
let menuHeight;

const STORAGE = window.localStorage;
const GLOBAL_LEADERBOARD = "resources/leaderboard.json";

// register service worker
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/pac-man/sw.js')
        .then(function(registration) {
            console.log('Registration successful, scope is:', registration.scope);
        })
        .catch(function(error) {
            console.log('Service worker registration failed, error:', error);
        });
}

// checks if user is on mobile
window.mobileAndTabletCheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

$(document).ready(() => {
    $("#loading").css("display", "none");
    game = new Game();
    menu = $("#menu");
    main = $("#main");

    // set clicking event handlers
    $("#start-game").click(() => {
        hideMenu();
        if (game.isPaused) game.continue();
        else game.ready();
    })
    $("#game-score").css("width", game.board.cells[0].length * CELL_SIZE + "px");
    $("#show-destinations").click((e) => {
        showDest = e.target.checked;
    })
    $("#play-looped-sounds").click((e) => {
        playLoopedSounds = e.target.checked;
    })
    $("#play-nonlooped-sounds").click((e) => {
        playNonLoopedSounds = e.target.checked;
    })

    // set menu position
    $(window).resize(setMenuMargin);
    setMenuMargin();

    // set online / offline check
    $(window).on("online",  updateOnlineStatus);
    $(window).on("offline", updateOnlineStatus);
    updateOnlineStatus();

    // historyAPI event handler
    $(window).on("popstate", (e) => {
        game.pause();
    })

    // input validation
    $("#name").keyup(validateName)
    validateName();

    updateLeaderboard();

    if (window.mobileAndTabletCheck()) {
        $("#mobile-controls").css("display", "block");
    }

    $(window).keydown((e) => {
        if (e.key === "e") {
            player = $("#easteregg")[0];
            if (player.paused) player.play();
            else player.pause();
        }
    })
});

function showMenu() {
    menu.show(300);
}

function hideMenu() {
    menu.hide(300);
}

function resetGame(eraseScore) {
    $(window).off("keydown.game");
    clearCanvas();
    game = new Game();
    game.isPaused = false;
    if (eraseScore) {
        lives = 2;
        score = 0;
        game.updateLives();
        game.updateScore();
    }
    else {
        if (score > STORAGE.getItem(name)) {
            STORAGE.setItem(name, score);
            updateLeaderboard();
        }
    }
}

function clearCanvas() {
    // this way the only reliable way to completely reset game canvas
    main.find("#game-canvas").remove();
    main.prepend("<canvas id='game-canvas'></canvas>")
}

function setMenuMargin() {
    if (window.innerWidth > 820) menu.css("margin", (main.outerHeight() - menu.outerHeight()) / 3 + "px 0 0 " + (main.outerWidth() - menu.outerWidth() + 15) / 2 + "px");
    else menu.css("margin", (main.outerHeight() - menu.outerHeight()) / 4 + "px 0 0 0");
    menu.css("height", window.getComputedStyle(document.querySelector("#menu"), null).getPropertyValue("height"));
    menuHeight = menu.css("height");
}

function updateOnlineStatus() {
    $("#status").html(navigator.onLine ? "Online" : "Offline");
    document.querySelector(":root").style.setProperty("--pseudo-color", navigator.onLine ? "greenyellow" : "red");
    $("#offline-sound").css("display", navigator.onLine ? "none" : "block");
    menu.css("height", "auto");
    setMenuMargin();
}

async function updateLeaderboard() {
    let data = [];
    let leaderboard = $("#leaderboard");

    // get values from local storage
    for (let i = 0; i < STORAGE.length; i++)
        if (STORAGE.key(i).length < 4)
            data.push({name: STORAGE.key(i), points: STORAGE.getItem(STORAGE.key(i)), isLocal: true})

    // get values from online leaderboards
    if (navigator.onLine) {
        let global = await loadFile(GLOBAL_LEADERBOARD);
        for (let gScore of global["scores"])
            data.push(gScore);
    }

    data.sort((a, b) => {
        if (a.points > b.points) return -1;
        if (a.points < b.points) return 1;
        return 0;
    });

    leaderboard.html("");
    if (data.length === 0) leaderboard.html("There is no<br>leaderboard data");
    for (let i = 0; i < Math.min(data.length, 15); i++) {
        if (data[i].isLocal) leaderboard.append("<p><span class='local-score'>" + data[i].name + ":</span> " + data[i].points + "</p>");
        else leaderboard.append("<p>" + data[i].name + ": " + data[i].points + "</p>");
    }
}

function validateName() {
    let re = /[a-zA-Z]{3}/;
    let n = $("#name");
    if (!re.test(n.val())) {
        n.css("border-bottom", "1px solid red");
        $("#start-game").attr("disabled", "");
    }
    else {
        n.css("border-bottom", "1px solid yellow");
        $("#start-game").removeAttr("disabled");
        name = n.val();
    }
}

function loadFile(filePath) {
    return $.get( filePath, data => {return data});
}