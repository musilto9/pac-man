// states: 0 = inactive, 1 = chase, 2 = scatter, 3 = frightened, 4 = end-frightened, 5 = dead

class Ghost {
    
    constructor() {
        this.color = "gray";
        this.x = 0;
        this.y = 0;
        this.direction = 3;
        this.offsetX = CELL_SIZE / 2;
        this.offsetY = 0;
        this.scatterX = -1;
        this.scatterY = -1;
        this.destX = -1;
        this.destY = -1;
        this.game = null;
        this.state = 0;
        this.speed = 0;
        this.timer = 0;
        this.dotCounter = 0;
        this.dotLimit = 0;
        this.switch = false;
        this.switchCounter = 0;
        this.isLeaving = false;
    }

    draw(ctx) {
        // switch tentacles type
        this.switchCounter++;
        if (this.switchCounter === 7) {
            this.switchCounter = 0;
            this.switch = !this.switch;
        }

        // body
        if (this.state === 3) ctx.fillStyle = "blue";
        else if (this.state === 4) ctx.fillStyle = "white";
        else if (this.state === 5) ctx.fillStyle = "transparent";
        else ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.arc((this.x + 0.5) * CELL_SIZE + this.offsetX,
            (this.y + 0.5) * CELL_SIZE + this.offsetY,
            0.9 * CELL_SIZE,
            Math.PI,
            0);
        let coords = [];
        if (this.switch) coords = [1.4, 1.1, 0.8, 0.5, 0.2, -0.1, -0.4];
        else coords = [1.1, 0.8, 0.5, 0.5, 0.5, 0.2, -0.1, -0.4];
        ctx.lineTo((this.x + coords[0]) * CELL_SIZE + this.offsetX, (this.y + 1.4) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[1]) * CELL_SIZE + this.offsetX, (this.y + 1) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[2]) * CELL_SIZE + this.offsetX, (this.y + 1.4) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[3]) * CELL_SIZE + this.offsetX, (this.y + 1) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[4]) * CELL_SIZE + this.offsetX, (this.y + 1.4) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[5]) * CELL_SIZE + this.offsetX, (this.y + 1) * CELL_SIZE + this.offsetY);
        ctx.lineTo((this.x + coords[6]) * CELL_SIZE + this.offsetX, (this.y + 1.4) * CELL_SIZE + this.offsetY);
        ctx.fill();

        // eyes
        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.ellipse(this.x * CELL_SIZE + this.offsetX,
            (this.y + 0.2) * CELL_SIZE + this.offsetY,
            CELL_SIZE/5,
            CELL_SIZE/3,
            0,
            0,
            2 * Math.PI);
        ctx.ellipse((this.x + 0.9) * CELL_SIZE + this.offsetX,
            (this.y + 0.2) * CELL_SIZE + this.offsetY,
            CELL_SIZE/5,
            CELL_SIZE/3,
            0,
            0,
            2 * Math.PI);
        ctx.fill();

        // destination indicator
        if (showDest) {
            ctx.strokeStyle = this.color;
            ctx.strokeRect(this.destX * CELL_SIZE, this.destY * CELL_SIZE, CELL_SIZE, CELL_SIZE);
        }

        // eyes
        if ([3, 5].includes(this.state)) ctx.fillStyle = "red";
        else ctx.fillStyle = "blue";
        ctx.beginPath();
        let r = 0.15 * CELL_SIZE;
        let s = 0;
        let e = 2 * Math.PI;
        switch (this.direction) {
            case 0:
                ctx.arc(this.x * CELL_SIZE + this.offsetX, this.y * CELL_SIZE + this.offsetY, r, s, e);
                ctx.arc((this.x + 0.9) * CELL_SIZE + this.offsetX, this.y * CELL_SIZE + this.offsetY, r, s, e);
                break;
            case 1:
                ctx.arc((this.x + 0.1) * CELL_SIZE + this.offsetX, (this.y + 0.2) * CELL_SIZE + this.offsetY, r, s, e);
                ctx.arc((this.x + 1) * CELL_SIZE + this.offsetX, (this.y + 0.2) * CELL_SIZE + this.offsetY, r, s, e);
                break;
            case 2:
                ctx.arc(this.x * CELL_SIZE + this.offsetX, (this.y + 0.4) * CELL_SIZE + this.offsetY, r, s, e);
                ctx.arc((this.x + 0.9) * CELL_SIZE + this.offsetX, (this.y + 0.4) * CELL_SIZE + this.offsetY, r, s, e);
                break;
            case 3:
                ctx.arc((this.x - 0.1) * CELL_SIZE + this.offsetX, (this.y + 0.2) * CELL_SIZE + this.offsetY, r, s, e);
                ctx.arc((this.x + 0.8) * CELL_SIZE + this.offsetX, (this.y + 0.2) * CELL_SIZE + this.offsetY, r, s, e);
                break;
        }
        ctx.fill();
    }

    move() {
        if (this.state === 0) return;
        switch(this.direction) {
            case 0:
                this.offsetY -= this.speed;
                if (this.offsetY <= -CELL_SIZE / 2) {
                    this.y--;
                    this.offsetY = CELL_SIZE / 2;
                }
                break;
            case 1:
                this.offsetX += this.speed;
                if (this.offsetX >= CELL_SIZE / 2) {
                    this.x++;
                    this.offsetX = -CELL_SIZE / 2;
                }
                break;
            case 2:
                this.offsetY += this.speed;
                if (this.offsetY >= CELL_SIZE / 2) {
                    this.y++;
                    this.offsetY = -CELL_SIZE / 2;
                }
                break;
            case 3:
                this.offsetX -= this.speed;
                if (this.offsetX <= -CELL_SIZE / 2) {
                    this.x--;
                    this.offsetX = CELL_SIZE / 2;
                }
                break;
        }

        // check if ghost is outside of game field
        if (this.x < 0) this.x = this.game.board.cells[0].length - 1;
        if (this.x >= this.game.board.cells[0].length) this.x = 0;

        // check ghost-pacman collision
        if (this.x === this.game.pacman.x && this.y === this.game.pacman.y) {
            if ([1, 2].includes(this.state)) this.game.pacman.die();
            else if(this.state !== 5) this.die();
        }

        // leave house sequence
        if (this.state === 5 && this.x === 13 && this.y === 15) {
            this.state = 0;
            this.speed = 0;
            this.offsetX = CELL_SIZE / 2;
            this.offsetY = 0;
            this.leaveHouse();
        }

        // calculate next destination
        if (this.offsetX === 0 && this.offsetY === 0) this.setNextDirection();
    }

    setNextDirection() {
        // this method is probably too overcomplicated, but it calculates the best next step when I only know current position and current destination
        let [x, y] = this.getDestination();
        this.destX = x;
        this.destY = y;
        let dist0 = (!this.isWall(this.x, this.y-1) && this.direction !== 2) ? Math.sqrt(Math.pow(Math.abs(this.x - x), 2) + Math.pow(Math.abs((this.y - 1) - y), 2)) : Infinity;
        let dist1 = (!this.isWall(this.x+1, this.y) && this.direction !== 3) ? Math.sqrt(Math.pow(Math.abs((this.x + 1) - x), 2) + Math.pow(Math.abs(this.y - y), 2)) : Infinity;
        let dist2 = (!this.isWall(this.x, this.y+1) && this.direction !== 0) ? Math.sqrt(Math.pow(Math.abs(this.x - x), 2) + Math.pow(Math.abs((this.y + 1) - y), 2)) : Infinity;
        let dist3 = (!this.isWall(this.x-1, this.y) && this.direction !== 1) ? Math.sqrt(Math.pow(Math.abs((this.x - 1) - x), 2) + Math.pow(Math.abs(this.y - y), 2)) : Infinity;

        if (([12, 15].includes(this.x) && [12, 24].includes(this.y))) dist0 = Infinity;

        if (dist0 === Math.min(dist0, dist1, dist2, dist3)) this.direction = 0;
        else if (dist3 === Math.min(dist0, dist1, dist2, dist3)) this.direction = 3;
        else if (dist2 === Math.min(dist0, dist1, dist2, dist3)) this.direction = 2;
        else if (dist1 === Math.min(dist0, dist1, dist2, dist3)) this.direction = 1;
    }

    getDestination() {
        if ([0, 1].includes(this.state)) return this.getEndPoint();
        else if (this.state === 2) return [this.scatterX, this.scatterY];
        else if ([3, 4].includes(this.state)) return [Math.random() * this.game.board.cells[0].length, Math.random() * this.game.board.cells.length]
        else if (this.state === 5) return [13, 15];
    }

    getEndPoint() {
        return [0, 0];
    }

    // sets ghost to frigntened state, which takes 7 seconds
    setFrightened() {
        if (this.state === 0) return;
        this.game.timerRunning = false;
        this.state = 3;
        this.speed = 0.5;
        setTimeout(() => {
            if (this.state !== 5) this.state = 4;
        }, 5000);
        setTimeout(() => {
            this.setStateByTimer();
            this.game.timerRunning = true;
        }, 7000);
    }

    // sets ghost to chase state
    setChase() {
        this.state = 1;
        this.speed = 1;
        this.direction = this.getOppositeDirection();
    }

    // sets ghost to scattered state
    setScatter() {
        this.state = 2;
        this.speed = 1;
        this.direction = this.getOppositeDirection();
    }

    // sets ghost to dead state
    setDead() {
        this.state = 5;
        this.speed = 1;
    }

    // switch that mimics original way of switching between ghost states
    setStateByTimer() {
        if (this.state === 0 || this.state === 5) return;
        if (this.timer <= 6 && this.state !== 2) this.setScatter();
        else if (this.timer > 6 && this.timer <= 26 && this.state !== 1) this.setChase();
        else if (this.timer > 26 && this.timer <= 33 && this.state !== 2) this.setScatter();
        else if (this.timer > 33 && this.timer <= 53 && this.state !== 1) this.setChase();
        else if (this.timer > 53 && this.timer <= 58 && this.state !== 2) this.setScatter();
        else if (this.timer > 58 && this.timer <= 78 && this.state !== 1) this.setChase();
        else if (this.timer > 78 && this.timer <= 83 && this.state !== 2) this.setScatter();
        else if (this.timer > 83 && this.state !== 1) this.setChase();
    }

    getOppositeDirection() {
        switch (this.direction) {
            case 0: return 2;
            case 1: return 3;
            case 2: return 0;
            case 3: return 1;
        }
    }

    leaveHouse() {
        // this method purposefully does not use regular pathfinding, because it would collide with house door, therefore it mimics ghost movement outside of house
        this.isLeaving = true;
        let i = setInterval(() => {
            if ((this.x + 0.5) * CELL_SIZE + this.offsetX === 14 * CELL_SIZE) {
                clearInterval(i);
                this.direction = 0;
                i = setInterval(() => {
                    if (this.y * CELL_SIZE + this.offsetY === 12 * CELL_SIZE) {
                        clearInterval(i);
                        this.x = 13;
                        this.y = 12;
                        this.offsetX = CELL_SIZE / 2;
                        this.offsetY = 0;
                        this.direction = 1;
                        this.setChase();
                        this.isLeaving = false;
                    }
                    this.offsetY += Math.sign(12 * CELL_SIZE - this.y * CELL_SIZE + this.offsetY)
                }, FRAME_DELAY);
            }
            this.offsetX += Math.sign(14 * CELL_SIZE - (this.x + 0.5) * CELL_SIZE + this.offsetX)
        }, FRAME_DELAY);
    }

    isWall(x, y) {
        if (this.state === 5) return this.game.board.isWallAlt(x, y);
        return this.game.board.isWall(x, y);
    }

    die() {
        this.setDead();
        this.game.pacman.kill();
    }
}



class Blinky extends Ghost {

    constructor() {
        super();
        this.x = 13;
        this.y = 12;
        this.direction = 3;
        this.color = "red";
        this.setScatter();
    }

    getEndPoint() {
        // always follows pacman
        return [this.game.pacman.x, this.game.pacman.y];
    }

    setScatterCoords() {
        this.scatterX = this.game.board.cells[0].length - 2;
        this.scatterY = 0;
    }
}

class Pinky extends Ghost {

    constructor() {
        super();
        this.x = 13;
        this.y = 15;
        this.direction = 0;
        this.color = "pink";
        this.dotLimit = 0;
    }

    setScatterCoords() {
        this.scatterX = 1;
        this.scatterY = 0;
    }

    getEndPoint() {
        // goes 4 blocks in front of pacman
        switch (this.game.pacman.direction) {
            case 0: return [this.game.pacman.x - 4, this.game.pacman.y - 4]; // this is true to the original overflow bug
            case 1: return [this.game.pacman.x + 4, this.game.pacman.y];
            case 2: return [this.game.pacman.x, this.game.pacman.y + 4];
            case 3: return [this.game.pacman.x - 4, this.game.pacman.y];
        }
    }
}

class Inky extends Ghost {

    constructor() {
        super();
        this.x = 11;
        this.y = 15;
        this.direction = 1;
        this.color = "aqua";
        this.dotLimit = 30;
    }

    setScatterCoords() {
        this.scatterX = this.game.board.cells[0].length - 1;
        this.scatterY = this.game.board.cells.length - 1;
    }

    getEndPoint() {
        // uses position of pacman and Blinky to calculate it's destination
        let resX, resY, pacX, pacY;
        switch (this.game.pacman.direction) {
            case 0:
                pacX = this.game.pacman.x - 2; // this is true to the original overflow bug
                pacY = this.game.pacman.y - 2;
                break;
            case 1:
                pacX = this.game.pacman.x + 2;
                pacY = this.game.pacman.y;
                break;
            case 2:
                pacX = this.game.pacman.x;
                pacY = this.game.pacman.y + 2;
                break;
            case 3:
                pacX = this.game.pacman.x - 2;
                pacY = this.game.pacman.y;
                break;
        }
        resX = (pacX - this.game.ghosts[0].x) * 2 + this.game.ghosts[0].x;
        resY = (pacY - this.game.ghosts[0].y) * 2 + this.game.ghosts[0].y;
        return [resX, resY];
    }
}

class Clyde extends Ghost {

    constructor() {
        super();
        this.x = 15;
        this.y = 15;
        this.direction = 3;
        this.color = "orange";
        this.dotLimit = 60;
    }

    setScatterCoords() {
        this.scatterX = 1;
        this.scatterY = this.game.board.cells.length - 1;
    }

    getEndPoint() {
        // follows pacman until it is 8 blocks away, then scatters
        if (Math.sqrt(Math.pow(Math.abs(this.x - this.game.pacman.x), 2) + Math.pow(Math.abs(this.y - this.game.pacman.y), 2)) > 8) {
            return [this.game.pacman.x, this.game.pacman.y]
        }
        return [this.scatterX, this.scatterY]
    }
}