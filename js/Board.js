// 0 = wall
// 1 = empty
// 2 = small dot
// 3 = big dot
// 4 = door

class Board {

    constructor() {
        this.cells = [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,   0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 3, 0, 1, 1, 0, 2, 0, 1, 1, 1, 0, 2, 0,   0, 2, 0, 1, 1, 1, 0, 2, 0, 1, 1, 0, 3, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,   2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0,   0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0,   0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 2, 2, 2, 2, 2, 2, 0, 0, 2, 2, 2, 2, 0,   0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 0],
            [0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0,   0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 0, 0, 0, 1, 0,   0, 1, 0, 0, 0, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 1, 0, 0, 0, 4,   4, 0, 0, 0, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1,   1, 1, 1, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0],

            [1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 0, 1, 1, 1,   1, 1, 1, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1],

            [0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1,   1, 1, 1, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 1, 0, 0, 0, 0,   0, 0, 0, 0, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 2, 0, 0, 1, 0, 0, 0, 0,   0, 0, 0, 0, 1, 0, 0, 2, 0, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0,   0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0],
            [0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,   0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0],
            [0, 3, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 1,   1, 2, 2, 2, 2, 2, 2, 2, 0, 0, 2, 2, 3, 0],
            [0, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0,   0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0],
            [0, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0,   0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0],
            [0, 2, 2, 2, 2, 2, 2, 0, 0, 2, 2, 2, 2, 0,   0, 2, 2, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0],
            [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0,   0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0],
            [0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,   2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ];
    }

    draw(ctx) {
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[0].length; j++) {
                switch (this.cells[i][j]) {
                    case 0:
                        this.drawWall(ctx, i, j, CELL_SIZE, CELL_SIZE);
                        break;
                    case 2:
                        ctx.fillStyle = "white";
                        ctx.fillRect((j + 0.4) * CELL_SIZE, (i + 0.4) * CELL_SIZE, CELL_SIZE * 0.2, CELL_SIZE * 0.2);
                        break;
                    case 3:
                        ctx.fillStyle = "white";
                        ctx.fillRect((j + 0.2) * CELL_SIZE, (i + 0.2) * CELL_SIZE, CELL_SIZE * 0.6, CELL_SIZE * 0.6);
                        break;
                    case 4:
                        ctx.strokeStyle = "white";
                        ctx.lineWidth = 3;
                        ctx.beginPath();
                        ctx.moveTo(j * CELL_SIZE, (i + 0.5) * CELL_SIZE);
                        ctx.lineTo((j + 1) * CELL_SIZE, (i + 0.5) * CELL_SIZE);
                        break;
                }
                ctx.stroke();
            }
        }
    }

    drawWall(ctx, i, j) {
        ctx.strokeStyle = "blue";
        ctx.lineWidth = 2;
        ctx.beginPath();

        // inner edges
        if (this.isWall(j, i-1) && this.isWall(j+1, i) && !this.isWall(j+1, i-1)) {
            ctx.arc((j + 1) * CELL_SIZE, i * CELL_SIZE, CELL_SIZE/ 2, Math.PI /2, Math.PI);
        }
        else if (this.isWall(j, i+1) && this.isWall(j+1, i) && !this.isWall(j+1, i+1)) {
            ctx.arc((j + 1) * CELL_SIZE, (i + 1) * CELL_SIZE, CELL_SIZE/ 2, Math.PI, Math.PI * 1.5);
        }
        else if (this.isWall(j, i+1) && this.isWall(j-1, i) && !this.isWall(j-1, i+1)) {
            ctx.arc(j * CELL_SIZE, (i + 1) * CELL_SIZE, CELL_SIZE/ 2, -Math.PI / 2, 0);
        }
        else if (this.isWall(j, i-1) && this.isWall(j-1, i) && !this.isWall(j-1, i-1)) {
            ctx.arc(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE/ 2, 0, Math.PI / 2);
        }

        // straight lines
        else if (this.isWall(j, i-1) && this.isWall(j, i+1)) {
            ctx.moveTo((j + 0.5) * CELL_SIZE, i * CELL_SIZE);
            ctx.lineTo((j + 0.5) * CELL_SIZE, (i + 1) * CELL_SIZE);
        }
        else if (this.isWall(j-1, i) && this.isWall(j+1, i)) {
            ctx.moveTo(j * CELL_SIZE, (i + 0.5) * CELL_SIZE);
            ctx.lineTo((j + 1) * CELL_SIZE, (i + 0.5) * CELL_SIZE);
        }

        // outer edges
        else if (this.isWall(j, i-1) && this.isWall(j+1, i)) {
            ctx.arc((j + 1) * CELL_SIZE, i * CELL_SIZE, CELL_SIZE / 2, Math.PI / 2, Math.PI);
        }
        else if (this.isWall(j, i+1) && this.isWall(j+1, i)) {
            ctx.arc((j + 1) * CELL_SIZE, (i + 1) * CELL_SIZE, CELL_SIZE / 2, Math.PI, Math.PI * 1.5);
        }
        else if (this.isWall(j, i+1) && this.isWall(j-1, i)) {
            ctx.arc(j * CELL_SIZE, (i + 1) * CELL_SIZE, CELL_SIZE / 2, -Math.PI / 2, 0);
        }
        else if (this.isWall(j, i-1) && this.isWall(j-1, i)) {
            ctx.arc(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE / 2, 0, Math.PI / 2);
        }

        // ends
        else if (this.isWall(j-1, i) && !this.isWall(j+1, i)) {
            ctx.moveTo(j * CELL_SIZE, (i + 0.5) * CELL_SIZE);
            ctx.lineTo((j + 0.5) * CELL_SIZE, (i + 0.5) * CELL_SIZE);
        }
        else if (this.isWall(j+1, i) && !this.isWall(j-1, i)) {
            ctx.moveTo((j+1) * CELL_SIZE, (i + 0.5) * CELL_SIZE);
            ctx.lineTo((j + 0.5) * CELL_SIZE, (i + 0.5) * CELL_SIZE);
        }
    }

    isWall(x, y) {
        return this.cells[y][x] === 0 || this.cells[y][x] === 4;
    }

    isWallAlt(x, y) {
        return this.cells[y][x] === 0;
    }

    isSmallDot(x, y) {
        return this.cells[y][x] === 2;
    }

    isBigDot(x, y) {
        return this.cells[y][x] === 3;
    }

    setCell(x, y, value) {
        this.cells[y][x] = value;
    }
}