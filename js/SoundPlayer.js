const GAME_START_AUDIO = new Audio("resources/sounds/game_start.wav");
const MUNCH1_AUDIO = new Audio("resources/sounds/munch_1.wav");
const MUNCH2_AUDIO = new Audio("resources/sounds/munch_2.wav");
const POWER_PELLET_AUDIO = new Audio("resources/sounds/power_pellet.wav");
const EAT_GHOST_AUDIO = new Audio("resources/sounds/eat_ghost.wav");
const EAT_FRUIT_AUDIO = new Audio("resources/sounds/eat_fruit.wav");
const RETREATING_AUDIO = new Audio("resources/sounds/retreating.wav");
const SIREN_AUDIO = new Audio("resources/sounds/siren_1.wav");
const DEATH_AUDIO = new Audio("resources/sounds/death_1.wav");
MUNCH1_AUDIO.onended = () => MUNCH2_AUDIO.play();

function playStateSounds() {
    if (!playLoopedSounds) return;
    if (allGhostsChase()){
        if (SIREN_AUDIO.paused){
            stopAllLoopingSounds();
            SIREN_AUDIO.play();
        }
    }
    else if (oneGhostDead()) {
        if (RETREATING_AUDIO.paused) {
            stopAllLoopingSounds();
            RETREATING_AUDIO.play();
        }
    }
    else if (oneGhostFrightened()) {
        if (POWER_PELLET_AUDIO.paused) {
            stopAllLoopingSounds();
            POWER_PELLET_AUDIO.play();
        }
    }
}

function stopAllLoopingSounds() {
    POWER_PELLET_AUDIO.pause();
    RETREATING_AUDIO.pause();
    SIREN_AUDIO.pause();
}

function allGhostsChase() {
    for (let i = 0; i < game.ghosts.length; i++) {
        if ([3, 4, 5].includes(game.ghosts[i].state)) return false;
    }
    return true;
}

function oneGhostDead() {
    for (let i = 0; i < game.ghosts.length; i++) {
        if (game.ghosts[i].state === 5) return true;
    }
    return false;
}

function oneGhostFrightened() {
    for (let i = 0; i < game.ghosts.length; i++) {
        if ([3, 4].includes(game.ghosts[i].state)) return true;
    }
    return false;
}