const TILESET = new Image(128, 16);
TILESET.src = "resources/tileset.png";
const FRUIT_SIZE = 16;

class Fruit {

    constructor(id) {
        this.id = id;
        this.points = fruits[id]["points"];
        this.x = 13;
        this.y = 18;
    }

    // draw image from tileset based on id of fruit
    draw(ctx) {
        ctx.drawImage(TILESET, this.id * FRUIT_SIZE, 0, FRUIT_SIZE, FRUIT_SIZE, CELL_SIZE * this.x - CELL_SIZE / 4, CELL_SIZE * this.y - CELL_SIZE / 4, CELL_SIZE * 1.5, CELL_SIZE * 1.5);
    }
}