const cachedFiles = [
    "index.html",
    "js/main.js",
    "js/Board.js",
    "js/Fruit.js",
    "js/Game.js",
    "js/Ghost.js",
    "js/Pacman.js",
    "js/SoundPlayer.js",
    "css/general.css",
    "css/leaderboard.css",
    "css/loading.css",
    "css/menu.css",
    "css/variables.css",
    "css/media.css",
    "resources/fruit-config.json",
    "resources/leaderboard.json",
    "resources/pacman.ico",
    "resources/tileset.png",
    "resources/PressStart2P-Regular.ttf",
    "resources/sounds/death_1.wav",
    "resources/sounds/easteregg.ogg",
    "resources/sounds/eat_fruit.wav",
    "resources/sounds/eat_ghost.wav",
    "resources/sounds/game_start.wav",
    "resources/sounds/munch_1.wav",
    "resources/sounds/munch_2.wav",
    "resources/sounds/power_pellet.wav",
    "resources/sounds/retreating.wav",
    "resources/sounds/siren_1.wav"
]

const staticCacheName = "swCache";

// This part of code is inspired by posts on developers.google.com

self.addEventListener('install', event => {
    self.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {return cache.addAll(cachedFiles)})
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                if (response) return response;
                return fetch(event.request)
                    .then(response => {
                        if (response.ok)
                            return caches.open(staticCacheName)
                                .then(cache => {
                                    cache.put(event.request.url, response.clone());
                                    return response;
                                });
                    });
            })
    );
});