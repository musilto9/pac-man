# Pac-Man

### POPIS PROJEKTU

Rozhodl jsem se vytvořit zjednodušenou moderní verzi jedné z nejkultovnějších her - Pac-Man.
Za cíl jsem si dal vytvořit jedu úroveň podle originální předlohy.
Podle originální předlohy jsou: mapa s pozicemi bodů, AI duchů, zvuky, vzhled a hodnota ovoce.
Některé věci jsem pro potřebu tohoto projektu upravil, např. princip generování ovoce.
Celkově jsem se snažil dát Pac-Manovi nový (modernější) vzhled.

Pro realizaci tohoto projektu jsem využíval technologie: HTML, CSS, JavaScript, jQuery, SVG.


### VÝVOJ

Už od začátku projektu jsem měl celkem jasnou představu o struktuře projektu, jelikož Pac-Man má poměrně jasně definované třídy a vztahy mezi nimi.
Díky tomu vznikla třídní struktura již na začátku projektu a příliš se nezměnila, až na pozdější přidání Fruit.js, ná které jsem si původně myslel, že nebudu mít čas.

Před začátkem implementace jsem věnoval dost času studiem článků o tom, jak funguje Pac-Man, což mi umožnilo věrohodně replikovat důležité elementy originálu.
Některé věci však na internetu popsaná nebyly, proto jsem si je musel vymyslet a nebo je reverse-engineerovat z online portů originální hry.

Po dodělání základní struktury hry jsem přešel o úroveň výš a začal se zabývat částmi jako je restartování hry, pozastavení hry, menu apod.
Zde jsem narazil na celkem velký problém s canvasem, konkrétně že se nechtěl resetovat do původní pozice. Nakonec jsem problém vyřešil smazáním a vytvořením nového canvasu.

Následně jsem se rozhodl přidat zvuky a zde jsem také narazil na problém, který se mi ale nepodařilo příliš vyřešit.
Konkrétně se mi nedaří zastavit přehrávající se zvuk, který generuji jako Audio() přímo v JavaScriptu. Naštěstí jsou všechny zvukové stopy krátkě, takže to nedělá žádný velký problém.

Dále jsem si začal hrát s tabulkou výsledků, do které jsem chtěl načítat online data.
To se mi částečně podařilo, respektive existuje JSON soubor, který reprezentuje online tabulku výsledků.
Do tohoto souboru však nemohu zapisovat, jelikož bych k tomu potřeboval aplikaci na serverové straně.
Co se mi však podařilo, je ukládat skóre lokálně u hráče s principem, že pokud hráč s daným jménem přesáhne své nejvyssí skóre, tak se tato hodnota přepíše.

Jelikož jedním z požadavků bylo, aby stránka fungovala i na mobilním zařízení, přišlo mi zajímavé přidat pro mobilní zařízení speciální ovládání pomocí tlačítek na obrazovce.
Tato tlačítka simulují kliknutí na klávesnici, takže jsem nepotřeboval nijak upravovat již napsané event handlery.

Abych v projektu využil i nějaké obrázky, rozhodl jsem se přidat do hry ovoce, zde jsem však narazil na problém, kvůli kterému jsem se musel odchýlit od originálu.
Jelikož má moje verze pouze jeden (první) level, znamenalo by to, že se pokaždé ukazuje stejné ovoce.
To jsem nechtěl, a proto jsem udělal výběr náhodného ovoce na základě vážených pravděpodobností pro každé ovoce. Tyto hodnoty jsou v příslušném konfiguračním souboru.

Na závěr jsem přidal service worker, který mi poskytnul možnost plnohodnotně využívat aplikaci offline, pokud jsem jí aspoň jednou navštívil v online stavu.


### OVLÁDÁNÍ

Pro ovládání pacmana využívám šipky na klávesnici. Je potřeba si dát pozor na správné načasování, jelikož okno na změnu směru na křižovatce je poměrně malé.
Originální verze si pravděpodobně zapamatuje poslední stisk klávesy a ten využije ve správný moment, tento princip jsem však neimplementoval, jelikož mi přišel zbytečný.

Pro pozastavení hry nebo pro její pokračování se dá využít klávesa Escape. Pro pozastavení hry se dá také využít tlačítko pro návrat na předchozí stránku.

Stisknutím klávesy E kdykoli mimo zadávání jména se spustí malé překvapení. Dalším stisknutím E se vypne.


### FUNKCE

Aplikace je validní podle validator.v3.org (kromě poznámky o viewportu, té se nemohu vyhnout, abych mohl zaručit korektní vzhled na mobilních zařízeních) a funguje ve všech moderních prohlížečích.

HTML 5
Semantické značky - main, aside a nav v index.html
Grafika SVG - loading pacman a logo v index.html
Média - Audio element easteregg v index.html
Formulářové prvky - zadávání jména v index.html
Offline aplikace - cachování pomocí service workeru v sw.js

CSS
Pokročilé selektory - before v leaderboard.css a "~" v menu.css
Vendor prefixy - transition v global.css
CSS3 transformace - translate v loading.css a media.css
CSS3 transitions/animations - transition téměř všude, animace v loading.css
Media queries - max-width v media.css

JavaScript
OOP přístup - třídní struktura ve složce js, dědění v Ghost.js
Framework - jQuery pro vyhledávání elementů v DOMu v main.js a Game.js
Pokročilá JS API - Local Storage na ukládání výsledků v main.js
Funkční historie - pozastavení hry tlačítkem zpět v main.js
Ovládání médií - funkce v SoundPlayer.js
Öffline aplikace - zobrazování stavu a stahování online leaderboardu v main.js
SVG - veškeré vykreslování (metody draw) ve složce js


### ZAJÍMAVOST

Při studiu fungování Pac-Mana jsem narazil na overflow bug, který se objevil v originální verzi při počítání cílové destinace pro některé duchy.
Tento bug způsobil, že když pacman směřoval nahoru, Pinkyho a Inkyho cílové destínace se posunuly o několik bloků doleva.
Pro legraci jsem se rozhodl tento bug záměrně naimplementovat a můžete ho vidět v metodě getEndPoint u těchto dvou duchů.
Detailní popis této a dalších chyb v originálním Pac-Manovi najdete zde https://tcrf.net/Bugs:Pac-Man_(Arcade)
